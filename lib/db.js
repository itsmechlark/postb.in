// ----------------------------------------------------------------------------

// npm
const yid = require('yid')
const ms = require('ms')
const slugit = require('slugit')
const level = require('level')

// local
const env = require('./env.js')

// ----------------------------------------------------------------------------
// setup

const thirtyMinsInMs = ms('30 mins')

const opts = {
  valueEncoding: 'json',
  createIfMissing: true,
  errorIfExists: false,
  compression: true
}
const db = level(env.dbDir, opts)

function createApiKey (apiId) {
  return `api~${apiId}`
}

function createBinKey (binId) {
  return `bin~${binId}`
}

function createReqKey (binId, reqId) {
  return `bin~${binId}~req~${reqId}`
}

function createReqRange (binId) {
  const key = createBinKey(binId) + '~req~'
  return {
    start: key,
    end: key + '~'
  }
}

// ----------------------------------------------------------------------------

function createApi (email, callback) {
  const apiId = yid() + '-' + slugit(email)
  const key = createApiKey(apiId)
  const now = Date.now()

  const val = {
    apiKey: apiId,
    email,
    now
  }

  db.put(key, val, function (err) {
    if (err) return callback(err)
    callback(null, val)
  })
}

function createBin (callback) {
  const binId = yid()
  const key = createBinKey(binId)
  const now = Date.now()

  const val = {
    binId,
    now,
    expires: now + ms('30 mins')
  }

  // insert this bin
  db.put(key, val, function (err) {
    if (err) return callback(err)
    callback(null, val)
  })
}

function getBin (binId, callback) {
  const key = createBinKey(binId)
  db.get(key, (err, data) => {
    if (err) {
      if (err.notFound) {
        return callback()
      }
      return callback(err)
    }
    callback(null, data)
  })
}

function delBin (binId, callback) {
  const key = createBinKey(binId)
  db.del(key, (err) => {
    if (err) return callback(err)

    // now to delete all requests in this bin
    const range = createReqRange(binId)
    db.createReadStream(range)
      .on('data', function (data) {
        db.del(data.key, (err) => {
          if (err) {
            console.log(`Error deleting ${data.key} - err:`, err)
          }
        })
      })
      .on('error', function (err) {
        console.log(`Error deleting requests for ${key} - err:`, err)
      })
      .on('end', function () {
        // console.log('Stream ended')
      })
      .on('close', function () {
        // even though we might still have outstanding requests to delete, we'll just callback now
        callback()
      })
  })
}

function insReq (binId, req, callback) {
  // set some info fields
  const now = Date.now()
  const reqId = yid()

  req.reqId = reqId
  req.binId = binId
  req.inserted = now

  // create the req key
  const key = createReqKey(binId, reqId)

  db.put(key, req, err => {
    if (err) return callback(err)
    callback(null, req)
  })
}

function getReq (binId, reqId, callback) {
  const key = createReqKey(binId, reqId)
  db.get(key, (err, data) => {
    if (err) {
      if (err.notFound) {
        return callback()
      }
      return callback(err)
    }
    callback(null, data)
  })
}

function shiftReq (binId, callback) {
  // find the first req and delete it
  const range = createReqRange(binId)
  let found = false
  let request = null

  db.createReadStream(range)
    .on('data', function (data) {
      if (!found) {
        found = true
        request = data.value
        db.del(data.key, (err) => {
          if (err) {
            console.log(`Error deleting ${data.key} - err:`, err)
          }
        })
      }
    })
    .on('error', function (err) {
      console.log(`Error shifting request for binId=${binId} - err:`, err)
      callback(err)
    })
    .on('end', function () {
      // console.log('Stream ended')
    })
    .on('close', function () {
      callback(null, request)
    })
}

function getReqsFor (binId, callback) {
  const range = createReqRange(binId)

  const reqs = []
  db.createReadStream(range)
    .on('data', function (data) {
      reqs.push(data.value)
    })
    .on('error', function (err) {
      console.log(`Error getting requests for binId=${binId} - err:`, err)
      callback(err)
    })
    .on('end', function () {
      // console.log('Stream ended')
    })
    .on('close', function () {
      // even though we might still have outstanding requests to delete, we'll just callback now
      callback(null, reqs)
    })
}

function cleanUp () {
  const now = Date.now()
  console.log('cleanUp() - now=' + now)

  let count = 0
  let seen = 0

  // const reqs = []
  db.createKeyStream()
    .on('data', function (key) {
      seen = seen + 1

      // the ID of this bin or req is always field 2
      const thisYid = key.split('~')[1]

      // date is always the first part of the yid
      const date = Number(thisYid.split('-')[0])

      const diff = now - date
      if (diff > thirtyMinsInMs) {
        db.del(key, err => {
          count = count + 1
          if (err) {
            console.warn('Error deleting key:', err)
          }
        })
      }
    })
    .on('end', function () {
      console.log(`Deleted ${count} of ${seen} records`)
    })
}

function close () {
  db.close()
}

// ----------------------------------------------------------------------------

module.exports = {
  // the db itself
  db,
  // API Keys
  createApi,
  // bins
  createBin,
  getBin,
  delBin,
  // requests
  insReq,
  getReq,
  shiftReq,
  getReqsFor,
  // clean up
  cleanUp,
  // misc
  close
}

// ----------------------------------------------------------------------------
