// --------------------------------------------------------------------------------------------------------------------

'use strict'

// local
var middleware = require('../middleware.js')
var db = require('../db.js')
var stats = require('../stats.js')
var pkg = require('../../package.json')

// --------------------------------------------------------------------------------------------------------------------

module.exports = function (app) {
  // just show the bin docs
  app.get(
    '/api/',
    function (req, res) {
      res.render('api', {
        title: 'API Docs - ' + pkg.title
      })
    }
  )
  // redirect to above
  app.get(
    '/api',
    middleware.redirectToSlash
  )

  // just show the bin info
  app.get(
    '/api/bin/:binId',
    middleware.getBinJson,
    function (req, res) {
      stats.apiGetBin.inc()
      return res.json(res.locals.bin)
    },
    middleware.jsonError
  )

  app.post(
    '/api/bin',
    function (req, res, next) {
      stats.apiCreateBin.inc()
      db.createBin(function (err, doc) {
        if (err) return next(err)
        return res.status(201).json(doc)
      })
    },
    middleware.jsonError
  )

  app.delete(
    '/api/bin/:binId',
    function (req, res, next) {
      stats.apiDelBin.inc()
      // don't need to do middleware to check this bin exists, since MongoDB will just say 'ok' anyway
      db.delBin(req.params.binId, function (err) {
        if (err) return next(err)
        res.json({ msg: 'Bin deleted' })
      })
    },
    middleware.jsonError
  )

  app.get(
    '/api/bin/:binId/req/shift',
    middleware.getBinJson,
    function (req, res, next) {
      stats.apiShiftReq.inc()
      db.shiftReq(res.locals.bin.binId, function (err, request) {
        if (err) return next(err)

        if (!request) {
          return res.status(404).json({ msg: 'No requests in this bin' })
        }

        res.json(request)
      })
    },
    middleware.jsonError
  )

  app.get(
    '/api/bin/:binId/req/:reqId',
    middleware.getBinJson,
    middleware.getReqJson,
    function (req, res) {
      stats.apiGetReq.inc()
      return res.json(res.locals.req)
    },
    middleware.jsonError
  )
}

// --------------------------------------------------------------------------------------------------------------------
