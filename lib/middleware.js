// ----------------------------------------------------------------------------

'use strict'

// core
const url = require('url')

// local
const db = require('./db.js')
const stats = require('./stats.js')

// ----------------------------------------------------------------------------
// setup

const oldBinIdRegex = /^[A-Za-z0-9]{8}$/

// ----------------------------------------------------------------------------

module.exports.redirectToSlash = function expressToSlashfunction (req, res) {
  const u = url.parse(req.originalUrl)
  res.redirect(302 /* FOUND */, u.pathname + '/' + (u.search || ''))
}

module.exports.jsonError = function (err, req, res, next) {
  /* eslint no-unused-vars:0 */
  console.warn(err)
  res.status(500).json({ msg: 'Internal Server Error' })
}

module.exports.getBin = function getBin (req, res, next) {
  const binId = req.params.binId
  if (oldBinIdRegex.test(binId)) {
    res.status(403).send('400 - Bad Request. This bin name is very old and should no longer be used.')
    return
  }

  db.getBin(req.params.binId, function (err, bin) {
    if (err) return next(err)

    if (!bin) {
      stats.apiGetBin404.inc()
      return res.status(404).send('404 - Not Found\n')
    }

    // all ok, save into res.locals
    res.locals.bin = bin
    next()
  })
}

module.exports.getBinJson = function getBin (req, res, next) {
  const binId = req.params.binId
  if (oldBinIdRegex.test(binId)) {
    res.status(403).json({ err: '400 - Bad Request. This bin name is very old and should no longer be used.'})
    return
  }

  db.getBin(req.params.binId, function (err, bin) {
    if (err) return next(err)

    if (!bin) {
      stats.apiGetBin404.inc()
      return res.status(404).json({ msg: 'No such bin' })
    }

    // all ok, save into res.locals
    res.locals.bin = bin
    next()
  })
}

module.exports.getReqJson = function getBin (req, res, next) {
  db.getReq(req.params.binId, req.params.reqId, function (err, request) {
    if (err) return next(err)

    if (!request) {
      stats.apiGetReq404.inc()
      return res.status(404).json({ msg: 'No such request' })
    }

    // all ok, save into res.locals
    res.locals.req = request
    next()
  })
}

// ----------------------------------------------------------------------------
